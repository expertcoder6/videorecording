package com.videorecordingtask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.iceteck.silicompressorr.SiliCompressor;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    VideoView mVideo;
    int REQUEST_CODE_VIDEO_CAPTURE = 2607;
    String filePath;
    TextView tvVideoSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_main);
        mVideo = findViewById(R.id.videView);
        tvVideoSize = findViewById(R.id.textView_videosize);
    }

    public void record_video(View view) {

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, REQUEST_CODE_VIDEO_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Uri videoUri = data.getData();
            mVideo.setVideoURI(videoUri);
            filePath = getVideoFilePath();
            new CompressVideo().execute("False", videoUri.toString(), filePath.toString());
            mVideo.start();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static String getVideoFilePath() {
        return getAndroidMoviesFolder().getAbsolutePath() + "/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + "videoRecorder.mp4";
    }

    public static File getAndroidMoviesFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
    }

    private class CompressVideo extends AsyncTask<String, String, String> {
        Dialog mDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // Toast.makeText(MainActivity.this, "Compressing.....", Toast.LENGTH_SHORT).show();
            mDialog = ProgressDialog.show(MainActivity.this,"","Compressing...");
        }

        @Override
        protected String doInBackground(String... strings) {
                String mVideoPath = null;
            try {
                Uri uri = Uri.parse(strings[1]);
                mVideoPath = SiliCompressor.with(MainActivity.this).compressVideo(uri,strings[2]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return mVideoPath;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mDialog.dismiss();
            File mFile = new File(s);
            Uri mUri = Uri.fromFile(mFile);
            exportMp4ToGallery(MainActivity.this,String.valueOf(mUri));
            float size  = mFile.length()/1024f;
            tvVideoSize.setText(String.format("Size : %.2f KB " , size));
        }
    }

    public static void exportMp4ToGallery(Context context, String filePath) {
        final ContentValues values = new ContentValues(2);
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
        values.put(MediaStore.Video.Media.DATA, String.valueOf(Uri.parse(filePath)));
        context.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,values);
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,Uri.parse("file://" + filePath)));
    }
}